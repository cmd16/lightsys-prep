import subprocess
'''
From https://ask.libreoffice.org/en/question/2641/convert-to-command-line-parameter/
soffice --headless --convert-to <TargetFileExtension>:<NameOfFilter> file_to_convert.xxx
soffice --headless --convert-to txt basic_test.odt
'''
arglist = ["soffice", "--headless", "--convert-to", "txt", "filename"]
filename = "LibreOfficeFiles/basic_test.odt"
arglist[4] = filename
arglist.extend(["--outdir", "LibreOfficeFiles"])
'''
From https://docs.python.org/3/library/subprocess.html: If check is true, and the process exits with a non-zero exit code, 
a CalledProcessError exception will be raised. 
Attributes of that exception hold the arguments, the exit code, and stdout and stderr if they were captured.
'''
try:
    subprocess.run(arglist, check=True)
except subprocess.CalledProcessError as e:
    # TODO: get this output of error messge to work
    print("return:", e.returncode, "error:", e.stderr)
